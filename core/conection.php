<?php
$db= new PDO('mysql:host=localhost;dbname=app-crud-php','root','');
if(!isset($_POST['name'])&&!isset($_POST['mail'])&&!isset($_POST['comment'])){
    header('Location: '.'http://localhost:8000');
}else{
    $name=$_POST['name'];
    $mail=$_POST['mail'];
    $comment=$_POST['comment'];
    if($name!=''&&$mail!=''&&$comment!=''){
        $smtp=$db->prepare("insert into app (name,mail,comment) values (?,?,?)");
        $smtp->bindParam(1,$name);
        $smtp->bindParam(2,$mail);
        $smtp->bindParam(3,$comment);
        if($smtp->execute()){
            echo json_encode('Registro agregado');
        }else{
            echo json_encode('Ha ocurrido un error');
        }
    }else{
        echo json_encode('Llene todos los campos');
    }
};